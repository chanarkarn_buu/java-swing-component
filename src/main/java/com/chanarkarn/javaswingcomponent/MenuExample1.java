/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

/**
 *
 * @author A_R_T
 */
public class MenuExample1 implements ActionListener {

    JFrame frame;
    JMenuBar menubar;
    JMenu file, edit, help;
    JMenuItem cut, copy, paste, selectAll;
    JTextArea textarea;

    MenuExample1() {
        frame = new JFrame();
        cut = new JMenuItem("cut");
        copy = new JMenuItem("copy");
        paste = new JMenuItem("paste");
        selectAll = new JMenuItem("selectAll");
        cut.addActionListener((ActionListener) this);
        copy.addActionListener(this);
        paste.addActionListener(this);
        selectAll.addActionListener(this);
        menubar = new JMenuBar();
        file = new JMenu("File");
        edit = new JMenu("Edit");
        help = new JMenu("Help");
        edit.add(cut);
        edit.add(copy);
        edit.add(paste);
        edit.add(selectAll);
        menubar.add(file);
        menubar.add(edit);
        menubar.add(help);
        textarea = new JTextArea();
        textarea.setBounds(5, 5, 360, 320);
        frame.add(menubar);
        frame.add(textarea);
        frame.setJMenuBar(menubar);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cut) {
            textarea.cut();
        }
        if (e.getSource() == paste) {
            textarea.paste();
        }
        if (e.getSource() == copy) {
            textarea.copy();
        }
        if (e.getSource() == selectAll) {
            textarea.selectAll();
        }
    }

    public static void main(String[] args) {
        new MenuExample1();
    }
}
