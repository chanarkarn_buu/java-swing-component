/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author A_R_T
 */
public class ButtonExample1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        final JTextField textfield = new JTextField();
        textfield.setBounds(50, 50, 150, 20);
        JButton button = new JButton("Click Here");
        button.setBounds(50, 100, 95, 30);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textfield.setText("Welcome to Javatpoint.");
            }
        });
        frame.add(button);
        frame.add(textfield);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
