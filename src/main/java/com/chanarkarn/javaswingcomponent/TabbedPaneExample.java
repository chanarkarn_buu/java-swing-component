/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 *
 * @author A_R_T
 */
public class TabbedPaneExample {

    JFrame frame;

    TabbedPaneExample() {
        frame = new JFrame();
        JTextArea ta = new JTextArea(200, 200);
        JPanel p1 = new JPanel();
        p1.add(ta);
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        JTabbedPane tabbedpane = new JTabbedPane();
        tabbedpane.setBounds(50, 50, 200, 200);
        tabbedpane.add("main", p1);
        tabbedpane.add("visit", p2);
        tabbedpane.add("help", p3);
        frame.add(tabbedpane);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new TabbedPaneExample();
    }
}
