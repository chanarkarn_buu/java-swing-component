/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author A_R_T
 */
public class LabelExample {

    public static void main(String args[]) {
        JFrame frame = new JFrame("Label Example");
        JLabel label1, label2;
        label1 = new JLabel("First Label.");
        label1.setBounds(50, 50, 100, 30);
        label2 = new JLabel("Second Label.");
        label2.setBounds(50, 100, 100, 30);
        frame.add(label1);
        frame.add(label2);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

}
