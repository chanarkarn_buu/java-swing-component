/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author A_R_T
 */
public class Simple {

    JFrame frame;

    Simple() {
        frame = new JFrame();//creating instance of JFrame  

        JButton button = new JButton("click");//creating instance of JButton  
        button.setBounds(130, 100, 100, 40);

        frame.add(button);//adding button in JFrame  

        frame.setSize(400, 500);//400 width and 500 height  
        frame.setLayout(null);//using no layout managers  
        frame.setVisible(true);//making the frame visible  
    }

    public static void main(String[] args) {
        new Simple();
    }

}
