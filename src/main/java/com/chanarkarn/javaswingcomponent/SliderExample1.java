/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author A_R_T
 */
public class SliderExample1 extends JFrame {

    public SliderExample1() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        slider.setMinorTickSpacing(2);
        slider.setMajorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        SliderExample frame = new SliderExample();
        frame.pack();
        frame.setVisible(true);
    }

    private void add(JPanel panel) {

    }
}
