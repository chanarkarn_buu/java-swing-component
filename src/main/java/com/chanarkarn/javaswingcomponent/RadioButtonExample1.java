/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author A_R_T
 */
public class RadioButtonExample1 extends JFrame implements ActionListener {

    JRadioButton radiobutton1, radiobutton2;
    JButton button;

    RadioButtonExample1() {
        radiobutton1 = new JRadioButton("Male");
        radiobutton1.setBounds(100, 50, 100, 30);
        radiobutton2 = new JRadioButton("Female");
        radiobutton2.setBounds(100, 100, 100, 30);
        ButtonGroup bg = new ButtonGroup();
        bg.add(radiobutton1);
        bg.add(radiobutton2);
        button = new JButton("click");
        button.setBounds(150, 150, 80, 50);
        button.addActionListener(this);
        add(radiobutton1);
        add(radiobutton2);
        add(button);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (radiobutton1.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Male.");
        }
        if (radiobutton2.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Female.");
        }
    }

    public static void main(String args[]) {
        new RadioButtonExample1();
    }
}
