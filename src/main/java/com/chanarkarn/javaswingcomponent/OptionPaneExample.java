/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author A_R_T
 */
public class OptionPaneExample {

    JFrame frame;

    OptionPaneExample() {
        frame = new JFrame();
        JOptionPane.showMessageDialog(frame, "Hello, Welcome to Javatpoint.");
    }

    public static void main(String[] args) {
        new OptionPaneExample();
    }
}
