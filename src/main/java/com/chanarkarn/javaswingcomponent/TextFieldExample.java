/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author A_R_T
 */
public class TextFieldExample {

    public static void main(String args[]) {
        JFrame frame = new JFrame("TextField Example");
        JTextField text1, text2;
        text1 = new JTextField("Welcome to Javatpoint.");
        text1.setBounds(50, 100, 200, 30);
        text2 = new JTextField("AWT Tutorial");
        text2.setBounds(50, 150, 200, 30);
        frame.add(text1);
        frame.add(text2);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

}
