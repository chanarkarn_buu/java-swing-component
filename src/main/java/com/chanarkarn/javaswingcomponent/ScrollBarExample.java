/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JScrollBar;

/**
 *
 * @author A_R_T
 */
public class ScrollBarExample {

    ScrollBarExample() {
        JFrame frame = new JFrame("Scrollbar Example");
        JScrollBar s = new JScrollBar();

        s.setBounds(100, 100, 50, 100);
        frame.add(s);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new ScrollBarExample();
    }
}
