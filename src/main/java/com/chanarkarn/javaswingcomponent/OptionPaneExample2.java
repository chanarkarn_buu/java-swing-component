/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author A_R_T
 */
public class OptionPaneExample2 {

    JFrame frame;

    OptionPaneExample2() {
        frame = new JFrame();
        String name = JOptionPane.showInputDialog(frame, "Enter Name");
    }

    public static void main(String[] args) {
        new OptionPaneExample2();
    }
}
