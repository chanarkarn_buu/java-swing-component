/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.javaswingcomponent;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;

/**
 *
 * @author A_R_T
 */
public class SeparatorExample1 {

    public static void main(String args[]) {
        JFrame frame = new JFrame("Separator Example");
        frame.setLayout(new GridLayout(0, 1));
        JLabel label1 = new JLabel("Above Separator");
        frame.add(label1);
        JSeparator sep = new JSeparator();
        frame.add(sep);
        JLabel label2 = new JLabel("Below Separator");
        frame.add(label2);
        frame.setSize(400, 100);
        frame.setVisible(true);
    }
}
